﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IsItWedding.Models;
using System.Net.Mail;
using System.Net;

namespace IsItWedding.Controllers
{
    public class WeddingController : Controller
    {
        private WeddingContext db = new WeddingContext();

        //
        // GET: /Wedding/

        public ActionResult Index()
        {
            return View(db.WeddingDays.ToList());
        }

        //
        // GET: /Wedding/Details/5

        public ActionResult Details(int id = 0)
        {
            //Data Access:
            WeddingDay weddingday = db.WeddingDays.Find(id);

            if (weddingday == null)
            {
                return HttpNotFound();
            }

            //Biznes Logic
            weddingday.IsItWeddingLiteral = weddingday.WeddingDate > DateTime.Now ? "NIE" : "TAK";

            return View(weddingday);
        }

        //
        // GET: /Wedding/Create

        public ActionResult Create()
        {
            WeddingDay initialData = new WeddingDay
            {
                GuestEmailList = new List<EmailAddress>
                {
                    new EmailAddress{},
                    new EmailAddress{}
                }
            };
            return View(initialData);
        }

        //
        // POST: /Wedding/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(WeddingDay weddingday)
        {
            if (ModelState.IsValid)
            {
                //Data access:
                db.WeddingDays.Add(weddingday);
                db.SaveChanges();

                //Buissiness Logic:
                SmtpClient client = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    EnableSsl = true,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential("isitwedding@gmail.com", "54321ytrewq")
                };

                MailMessage mail = new MailMessage
                {
                    From = new MailAddress("isitwedding@gmail.com"),
                    Subject = "Uwaga biorę ślub!!!",
                    Body = string.Format("{0} i {1} biorą ślub: {2}",
                    weddingday.BrideName,
                    weddingday.GroomName,
                    weddingday.WeddingDate.Value.ToShortDateString())
                };

                weddingday.GuestEmailList
                    .Where(p => p.Address != null)
                    .ToList()
                    .ForEach(email =>
                {
                    mail.To.Add(email.Address);
                });

                client.Send(mail);

                return RedirectToAction("Index");
            }

            return View(weddingday);
        }

        //
        // GET: /Wedding/Edit/5

        public ActionResult Edit(int id = 0)
        {
            WeddingDay weddingday = db.WeddingDays.Find(id);
            if (weddingday == null)
            {
                return HttpNotFound();
            }
            return View(weddingday);
        }

        //
        // POST: /Wedding/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(WeddingDay weddingday)
        {
            if (ModelState.IsValid)
            {
                db.Entry(weddingday).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(weddingday);
        }

        //
        // GET: /Wedding/Delete/5

        public ActionResult Delete(int id = 0)
        {
            WeddingDay weddingday = db.WeddingDays.Find(id);
            if (weddingday == null)
            {
                return HttpNotFound();
            }
            return View(weddingday);
        }

        //
        // POST: /Wedding/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            WeddingDay weddingday = db.WeddingDays.Find(id);
            db.WeddingDays.Remove(weddingday);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}