namespace IsItWedding.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.WeddingDays", "IsItWeddingLiteral", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.WeddingDays", "IsItWeddingLiteral");
        }
    }
}
