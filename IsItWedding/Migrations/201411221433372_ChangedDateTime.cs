namespace IsItWedding.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedDateTime : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.WeddingDays", "WeddingDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.WeddingDays", "WeddingDate", c => c.DateTime(nullable: false));
        }
    }
}
