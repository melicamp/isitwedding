namespace IsItWedding.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedGuestList : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EmailAddresses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Address = c.String(),
                        WeddingDay_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.WeddingDays", t => t.WeddingDay_Id)
                .Index(t => t.WeddingDay_Id);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.EmailAddresses", new[] { "WeddingDay_Id" });
            DropForeignKey("dbo.EmailAddresses", "WeddingDay_Id", "dbo.WeddingDays");
            DropTable("dbo.EmailAddresses");
        }
    }
}
