namespace IsItWedding.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CascadeDelete : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.EmailAddresses", "WeddingDay_Id", "dbo.WeddingDays");
            DropIndex("dbo.EmailAddresses", new[] { "WeddingDay_Id" });
            AlterColumn("dbo.EmailAddresses", "WeddingDay_Id", c => c.Int(nullable: false));
            AddForeignKey("dbo.EmailAddresses", "WeddingDay_Id", "dbo.WeddingDays", "Id", cascadeDelete: true);
            CreateIndex("dbo.EmailAddresses", "WeddingDay_Id");
        }
        
        public override void Down()
        {
            DropIndex("dbo.EmailAddresses", new[] { "WeddingDay_Id" });
            DropForeignKey("dbo.EmailAddresses", "WeddingDay_Id", "dbo.WeddingDays");
            AlterColumn("dbo.EmailAddresses", "WeddingDay_Id", c => c.Int());
            CreateIndex("dbo.EmailAddresses", "WeddingDay_Id");
            AddForeignKey("dbo.EmailAddresses", "WeddingDay_Id", "dbo.WeddingDays", "Id");
        }
    }
}
