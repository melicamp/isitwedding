﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace IsItWedding.Models
{
    public class WeddingContext : DbContext
    {
        public WeddingContext()
            : base("DefaultConnection")
        {
        }

        public DbSet<WeddingDay> WeddingDays { get; set; }
        public DbSet<EmailAddress> EmailAddresses { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<WeddingDay>()
                .HasMany(p => p.GuestEmailList)
                .WithRequired();
        }
    }

    public class WeddingDay
    {
        public int Id { get; set; }
        [Display(Name = "Panna młoda")]
        public string BrideName { get; set; }
        [Display(Name = "Pan młody")]
        public string GroomName { get; set; }
        [Display(Name = "Data ślubu")]
        public DateTime? WeddingDate { get; set; }
        [Display(Name="Czy to ślub?")]
        public string IsItWeddingLiteral { get; set; }
        [Display(Name = "Lista email gości")]
        public List<EmailAddress> GuestEmailList { get; set; }
    }
}