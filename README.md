# README #

This README documents whatever steps are necessary to get this application up and running.

### What is this repository for? ###

* Example application in ASP.NET MVC shows how to turn monolithic application into n-tier

### How do I get set up? ###

* Created in Visual Studio 2013 Community Edition
* Requires Sql Server (tested on Sql Server 2012)

1. Restore nuget packages in package manager console. 
2. Put connection string for your Sql Server database into web.config file in <connectionStrings> section under 'DefaultConnection'. 
   Database doesn't have to exists yet, just specify a name for it.
3. Build solution.
4. Restart Visual Studio. 
5. Run application from Visual Studio(F5).  

### View steps of creating 3-tier ###

To view steps of process of creating 3-tier application first checkout and than switch to different branches:

* 0-step-monolithic -> first version of app
* 1-step-bizlogic -> extracted some bizlogic to new project
* 2-step-data-access -> extracted data-access to new project. App is now 3-tier. 

### Database ###

Database needs updates and sometimes recreation (after switching from branch 2-step-data-access back to 0-step-monolithic).

To recreate database delete it and run Update-Database in package manager console.

Whenever you see an error that states that databse needs update run Update-Database in package manager console. 

When on branch 2-step-data-access you need to run Update-Database -p IsItWedding.DataAccess since context is now in new project.